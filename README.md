# S.O.L.I.D Principles explained in Python with examples.

SOLID is the acronym for a collection of 5 object-oriented design principles, first conceptualised by Robert C. Martin about 20 years ago.

The letters stand for:
1. **S**ingle Responsibility Principle
2. **O**pen/Closed Principle
3. **L**iskov Substitution Principle
4. **I**nterface Segregation Principle
5. **D**ependency Inversion Principle

It is really valuable when writing industry-standard code.

-----
## 1. Single Responsibility Principle
> “The Single Responsibility Principle requires that each class is responsible for only one thing.“

Let's take a look at an example below.
```python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self) -> str:
        pass

    def save(self, animal: Animal):
        pass
```
Here the Animal class violates the SRP.

SRP states that classes should have one responsibility, here, we can draw out two responsibilities: animal database management and animal properties management. The constructor and get_name manage the Animal properties while the save manages the Animal storage on a database.

To make this conform to SRP, we create another class that will handle the sole responsibility of storing an animal to a database:


```python
class Animal:
    def __init__(self, name: str):
        self.name = name
    
    def get_name(self):
        pass


class AnimalDB:
    def get_animal(self) -> Animal:
        pass

    def save(self, animal: Animal):
        pass
```
-----
## 2. Open-Closed Principle

> Software entities(classes, modules, functions) should be open for extension, not modification.

Let’s imagine you have a store, and you give a discount of 20% to your favorite customers using this class.

When you decide to offer double the 20% discount to VIP customers. You may modify the class like this:

```python
class Discount:
    def __init__(self, customer, price):
        self.customer = customer
        self.price = price

    def give_discount(self):
            if self.customer == 'fav':
                return self.price * 0.2
            if self.customer == 'vip':
                return self.price * 0.4
```

This fails the OCP principle because OCP forbids it.

If we want to give a new percent discount maybe, to a different type of customers, you will see that a new logic will be added.
To make it follow the OCP principle, we will add a new class that will extend the Discount. 

In this new class, we would implement its new behavior :

```python
class Discount:
    def __init__(self, customer, price):
        self.customer = customer
        self.price = price

    def get_discount(self):
            return self.price * 0.2


class VIPDiscount(Discount):
    def get_discount(self):
        return super().get_discount() * 2
```

If you decide 80% discount to super VIP customers, it should be like this:
You see, extension without modification.

```python
class SuperVIPDiscount(VIPDiscount):
    def get_discount(self):
        return super().get_discount() * 2
```

---
## 3. Liskov Substitution Principle

> “Objects in a program should be replaceable with instances of their base types without altering the
correctness of that program.”

```python
def animal_leg_count(animals: list):
    for animal in animals:
        if isinstance(animal, Lion):
            print(lion_leg_count(animal))
        elif isinstance(animal, Mouse):
            print(mouse_leg_count(animal))
        elif isinstance(animal, Pigeon):
            print(pigeon_leg_count(animal))
        
animal_leg_count(animals)
```

To make this function follow the LSP principle, we will follow this LSP requirements postulated by Steve Fenton:

If the super-class (Animal) has a method that accepts a super-class type (Animal) parameter. 

Its sub-class(Pigeon) should accept as argument a super-class type (Animal type) or sub-class type(Pigeon type).

If the super-class returns a super-class type (Animal). Its sub-class should return a super-class type (Animal type) or sub-class type(Pigeon).

Now, we can re-implement animal_leg_count function:


```python
def animal_leg_count(animals: list):
    for animal in animals:
        print(animal.leg_count())
        
animal_leg_count(animals)
```

The animal_leg_count function cares less the type of Animal passed, it just calls the leg_count method. 

All it knows is that the parameter must be of an Animal type, either the Animal class or its sub-class.

The Animal class now have to implement/define a leg_count method.

And its sub-classes have to implement the leg_count method:


```python
class Animal:
    def leg_count(self):
        pass


class Lion(Animal):
    def leg_count(self):
        pass
```


When it’s passed to the animal_leg_count function, it returns the number of legs a lion has.

You see, the animal_leg_count doesn’t need to know the type of Animal to return its leg count, 

it just calls the leg_count method of the Animal type because by contract a sub-class of Animal class must implement the leg_count function.

---
## 4. Interface Segregation Principle

> “Many client-specific interfaces are better than one general-purpose interface.”

Let’s look at the below IShape interface:

```python
class IShape:
    def draw_square(self):
        raise NotImplementedError
    
    def draw_rectangle(self):
        raise NotImplementedError
    
    def draw_circle(self):
        raise NotImplementedError
```
This interface draws squares, circles, rectangles. 

Class Circle, Square or Rectangle implementing the IShape 
interface must define the methods draw_square(), draw_rectangle(), draw_circle().

If we add another method to the IShape interface, like draw_triangle(),

```python
class IShape:
    def draw_square(self):
        raise NotImplementedError
    
    def draw_rectangle(self):
        raise NotImplementedError
    
    def draw_circle(self):
        raise NotImplementedError
    
    def draw_triangle(self):
        raise NotImplementedError
```
This fails the ISP principle.

ISP states that interfaces should perform only one job (just like the SRP principle) any extra grouping of behavior should be abstracted away to another interface.

To make our IShape interface conform to the ISP principle, we segregate the actions to different interfaces.

Classes (Circle, Rectangle, Square, Triangle, etc) can just inherit from the IShape interface and implement their own draw behavior.

```python
class IShape:
    def draw(self):
        raise NotImplementedError

class Circle(IShape):
    def draw(self):
        pass

class Square(IShape):
    def draw(self):
        pass

class Rectangle(IShape):
    def draw(self):
        pass
```

We can then use the I -interfaces to create Shape specifics like Semi Circle, Right-Angled Triangle, Equilateral Triangle, Blunt-Edged Rectangle, etc.

---
## 5. Dependency Inversion Principle
>a. "High-level modules should not depend on low-level modules. Both should depend on abstractions."

>b. "Abstractions should not depend on details. Details should depend on abstractions.”

The general idea of this principle is: High-level modules, which provide complex logic, should be easily reusable and unaffected by changes in low-level modules, which provide utility features. 
To achieve that, you need to introduce an abstraction that decouples the high-level and low-level modules from each other.

Let's have a look on following code snippet:

```python
class HTTPConnection():
    pass

class HTTP:
    def __init__(self, http_service: HTTPConnection):
        self.http_service = xml_http_service

    def get(self, url: str):
        self.http_service.request(url, 'GET')
```

This code will through error when we call get() method since HTTP is depend on HTTPConnection and HTTPConnection have no method implemented called request(). So in a nutshell, we’ve tightly coupled two classes together and DIP prohibit it. 

As per DIP HTTP shouldn’t care what type of connection we are using, it only care that a connection that has request() method implemented.

To comply with DIP we can redesign our class like this.

```python
from abc import ABC, abstractmethod  
    
class Connection(ABC):
    @abstractmethod	
    def request(self, url: str, **kwargs):
        raise NotImplementedError
	
class HTTPConnection():
    def request(self, url: str, options:dict):
        pass
	
class HTTP:
    def __init__(self, http_service: Connection):
        self.http_service = http_service

    def get(self, url: str):
        self.http_service.request(url, 'GET')
```

Now we can see that, HTTP class (high level module) depends on the Connection interface (abstraction) and the HTTPConnection (low level modules), depends on the Connection interface (abstraction).

### Reference :-
1. https://gist.github.com/dmmeteo/f630fa04c7a79d3c132b9e9e5d037bfd
2. http://shubho.me/2019-09-15-solid/
3. https://levelup.gitconnected.com/s-o-l-i-d-principles-explained-in-python-with-examples-83b2b43bdcde